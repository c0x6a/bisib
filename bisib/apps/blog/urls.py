# -*- coding: UTF-8 -*-
from django.conf.urls import patterns, url
from apps.blog import views


urlpatterns = patterns(
    '',
    url(r'^$', views.home_blog),
    url(r'configure/$', views.configure_blog),
)
