"""
Model forms to manage blog
"""
from django.forms import ModelForm, TextInput
from apps.blog.models import Blog


class BlogForm(ModelForm):
    class Meta:
        model = Blog
        # widgets = {
        #     'name': TextInput(attrs={'required': ''}),
        #     'description': TextInput(attrs={'required': ''}),
        #     'url': TextInput(attrs={'required': ''}),
        # }
