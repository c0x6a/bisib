from apps.blog.models import Blog


class BlogConfig():
    """
    Configuration parameters of BISIB
    """
    try:
        __blog_config = Blog.objects.get(id=1)
    except Blog.DoesNotExist:
        __blog_config = None

    def logo(self):
        if self.__blog_config:
            return self.__blog_config.logo
        else:
            return 'no-logo'

    def name(self):
        if self.__blog_config:
            return self.__blog_config.name
        else:
            return 'no-name'

    def description(self):
        if self.__blog_config:
            return self.__blog_config.description
        else:
            return 'no-description'

    def url(self):
        if self.__blog_config:
            return self.__blog_config.url
        else:
            return 'no-url'
