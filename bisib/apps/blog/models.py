# from django.conf import settings
from django.db import models


# Create your models here.
class Blog(models.Model):
    name = models.CharField(
        max_length=100, verbose_name='Blog name')
    description = models.CharField(
        max_length=200, verbose_name='Blog description')
    logo = models.FileField(
        upload_to='img/blog', verbose_name='Blog logo')
    url = models.CharField(
        max_length=100, verbose_name='Blog URL')

    def __str__(self):
        return self.name
