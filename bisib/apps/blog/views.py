from django.http.response import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from apps.blog.blog_config import BlogConfig
from apps.blog.forms import BlogForm
from apps.blog.models import Blog
from apps.posts.views import home as home_posts


# Create your views here.
def home_blog(request):
    """
    Check if blog is already configured
    """
    try:
        this_blog = Blog.objects.get(id=1)
    except Blog.DoesNotExist:
        return HttpResponseRedirect('/blog/configure/')

    return home_posts(request)


def configure_blog(request):
    """
    Configure BISIB for the first time
    """
    configure_form = BlogForm()

    if request.method == 'POST':
        configure_form = BlogForm(request.POST, request.FILES)

        if configure_form.is_valid():
            configure_form.save()

    data = {
        'configure_form': configure_form,
        'blog_data': BlogConfig()
    }

    return render_to_response(
        'blog_configure.html', data, context_instance=RequestContext(request))
