from django.contrib import admin

# Register your models here.
from apps.blog.models import Blog

admin.site.register(Blog)
