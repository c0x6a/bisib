from django.shortcuts import render_to_response
from django.template.context import RequestContext

from apps.blog.blog_config import BlogConfig
from apps.posts.models import Post


def home(request):
    """
    Lis all post from blog
    """
    posts = Post.objects.filter(published=True).order_by('-post_date')
    blog = BlogConfig()

    data = {
        'posts': posts,
        'blog_config': blog
    }

    return render_to_response(
        'list_posts.html', data, context_instance=RequestContext(request))


def view_post(request, post_year, post_month, post_title):
    """
    View a selected post
    """
    post = Post.objects.get(post_date__year=post_year,
                            post_date__month=post_month,
                            title_url=post_title)
    blog = BlogConfig()

    data = {
        'post': post,
        'blog_config': blog
    }

    return render_to_response(
        'view_post.html', data, context_instance=RequestContext(request))


# def add_modify_post(request, post_year=None, post_month=None, post_title=None):
#     """
#     Add or modify a post
#     """
#     # Modify a post
#     post = None
#     try:
#         post = Post.objects.get(post_date__year=post_year,
#                                 post_date__month=post_month,
#                                 title_url=post_title)
#     except Post.DoesNotExist:
#         Add new post from HTML form
#         if request.method == 'POST':
#             post = post_form()
