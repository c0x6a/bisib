# -*- coding: UTF-8 -*-
"""
Models to manage posts
"""
from django.db import models
from ckeditor.fields import RichTextField


class Post(models.Model):
    """
    Store posts of the blog
    """
    title = models.CharField(
        max_length=50, verbose_name='Post Title')
    content = RichTextField()
    post_date = models.DateTimeField(auto_now=True)
    published = models.BooleanField(default=False)
    title_url = models.CharField(
        max_length=50, null=True, blank=True, unique=True)

    def __str__(self):
        return self.title

    def save(self):
        self.title_url = self.title.replace(' ', '_')
        super(Post, self).save()
