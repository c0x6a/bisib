"""
Model forms to manage posts
"""

from django.forms.models import ModelForm
from apps.posts.models import Post


class PostForm(ModelForm):
    class Meta:
        model = Post
