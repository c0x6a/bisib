# -*- coding: UTF-8 -*-
from django.conf.urls import patterns, url
from apps.posts import views


urlpatterns = patterns(
    '',
    url(r'^$', views.home),
    url(r'^(?P<post_year>\d{4})/(?P<post_month>\d{1,2})/(?P<post_title>\w+)/$',
        views.view_post, name='view_post'),
)
