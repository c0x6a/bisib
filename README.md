# BISIB

BISIB Is a Simple and Intuitive Blogware, made with Django 1.6

## Authors
** Carlos Joel Delgado Pizarro **

+ <http://code.carlosjoel.net>
+ <https://plus.google.com/+CarlosJoelDelgadoPizarro/>


## changelog
### v0.1
* Initial project
* Just a simple content managment using Django Admin
